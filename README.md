# Gruvbox V

A minimalist gruvbox theme for [Zola][zola].

## Credit

Thanks to [zerm][zerm] zola theme template, I was able to take inspiration from
it and used some concepts and code over.

[zola]: https://getzola.org/
[zerm]: https://github.com/ejmg/zerm

